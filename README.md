# Social Network

## Prérequis
- npm@6.9.0

## Installation (back et front)
`npm install` dans le dossier back et front

## Test

## Serveur de test (back et front)
`npm start` dans le dossier back et front

## Fonctionnalités
- Création d'utilisateur
- Connexion/Deconnexion
- Mot de passe oublié avec envoie de mail
- Vu des profil léger en allant sur la page /profile/<id_user>
- Chat entre les deux utilisateurs déjà créé (Ne fonctionne pas avec de nouveau utilisateur)

/!\ Pour que l'envoi de mail fonctionne merci e renseigner un email correct dans le ficheir config/config.js

## Utilisateurs déjà présent
### User 1
>email : vlabarre5@gmail.com

>password : test

### User 2
>email : test@test.fr

>password : test
