import React, { Component } from "react";
import './Chat.css';
import { picturesUrl } from "../../../config/connexion_back";
import user from "../../services/user/User";
import TextField from "@material-ui/core/TextField/TextField";
import Socket from '../../services/chatroom/Socket';

class Onglet extends Component {

  constructor(props) {
    super(props);
    this.state = {
      show: props.show,
      title: props.title,
      to: props.to,
      messages: [],
      newMessage: ""
    }
  }

  componentWillMount() {
    this.socket = new Socket(user.get_connected_user(), this._handleCatchMessage);
  }

  componentWillUpdate(props) {
    const { show } = this.state;

    if(show !== props.show) {
      this.getMessages(this.props.talk).then(messages => {
        this.setState({show: props.show, messages});
      });
    }
  }

  getMessages(talkId) {
    return new Promise(function(resolve) {
      user.get_messages(talkId).then(messages => {
        resolve(messages);
      });
    })
  };

  // Arrow fx for bind
  _handleCatchMessage = (socket) => {
    const { talk } = this.props;

    socket.on('message', (message) => {
      let { messages } = this.state;

      const newMessage = {
        text: message.message,
        from: message.from
      };

      user.save_message_in_talk(newMessage, talk);

      messages.reverse().push(newMessage);
      messages.reverse();

      this.setState(messages);
    });
  };

  // Arrow fx for bind
  _handleTextFieldChange = (event) => {
    this.setState({
      newMessage: event.target.value
    });
  };

  // Arrow fx for bind
  _handleSubmit = (event) => {
    const { newMessage, to, messages } = this.state;
    event.preventDefault();

    let newMessageObject = {
      from: user.get_connected_user()._id,
      text: newMessage
    };

    this.socket.message(user.get_connected_user()._id, to, newMessage);

    messages.reverse().push(newMessageObject);
    messages.reverse();
    event.target.reset();
    this.setState({
      newMessage: ""
    });
  };

  render() {
    const { title, messages, show } = this.state;
    const { number, handleDisplayMessage, picture } = this.props;

    const ongletStyle = {right: 'calc(15% + 40px + ' + number * 270 + 'px)'};

    return (
      <div id="chat" className={show} style={ongletStyle}>
        <header onClick={() => handleDisplayMessage(number)}>
          <div className="header-contain">
            <img aria-label="Recipe" className="avatar" alt="avatar" src={ picturesUrl + picture }/>
            <p>{ title }</p>
          </div>
        </header>
        <section id="chat-messages">
          <div className="chat-messages-content">
            { messages.map((message, index) =>
              <div key={ index } className="message-single">
                { message.from === user.get_connected_user()._id ? (
                    <p className="right">{ message.text }</p>
                  ) : (
                    <p className="left">{ message.text }</p>
                  )
                }
              </div>
            )}
          </div>
          <form className="chat-form" onSubmit={(event) => this._handleSubmit(event)}>
            <TextField
              id="outlined-bare"
              placeholder="Écrivez un message ..."
              margin="none"
              onChange={(event) => this._handleTextFieldChange(event)}
              fullWidth
            />
          </form>
        </section>
      </div>
    )
  }

}

export default Onglet;
