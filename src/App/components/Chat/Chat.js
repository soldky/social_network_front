import React, { Component } from "react";
import './Chat.css';
import { picturesUrl } from "../../../config/connexion_back";

// import service managers
import user from "../../services/user/User";
import Onglet from "./Onglet";

class Chat extends Component {

  state = {
    talks: [],
    ongletOpen: 0
  };

  componentWillMount() {
    this.getTalks();
  }

  getTalks() {
    let { talks } = this.state;

    const connected_user = user.get_connected_user();

    connected_user.talks.forEach((talk) => {
      talk.contributors.forEach((contributor) => {
        if(connected_user._id !== contributor) {
          user.get_user(contributor).then(receiver => {
            const name = receiver.name.firstname + " " + receiver.name.lastname;
            talks.push({
              id: talk._id,
              title: name,
              messages: talk.messages.reverse(),
              onglet: "hidden",
              number: 0,
              to: contributor,
              picture: receiver.picture
            });
            this.setState({talks: talks});
          });
        }
      });
    });
  };

  // Arrow fx for bind
  _handleTalkOnglet = (index) => {
    let { talks, ongletOpen } = this.state;

    if(talks[index].onglet === "hidden") {
      ongletOpen++;
      talks[index].number = ongletOpen;
      talks[index].onglet = "visible";
    }

    else {
      talks[index].onglet = "hidden";
      ongletOpen--;
      talks.forEach(function (talk) {
        if(talk.number > talks[index].number) {
          talk.number--;
        }
      });
    }
    this.setState({talks, ongletOpen});
  };

  // Arrow fx for bind
  _handleDisplayMessage = (number) => {
    let { talks } = this.state;

    talks.forEach(function(talk) {
      if(talk.number === number + 1) {
        if(talk.onglet === "visible") {
          talk.onglet = "reduce";
        }
        else if(talk.onglet === "reduce") {
          talk.onglet = "visible";
        }
        else {
          talk.onglet = "reduce";
        }
      }
    });

    this.setState({talks});
  };

  render() {
    const { talks } = this.state;

    return (
      <div id="chatbox">
        <div id="list-users">
          { talks.map((talk, index) =>
            <div key={index}>
              <div className="user" onClick={ () => this._handleTalkOnglet(index) }>
                <div className="icon">
                  <img className="avatar" alt="avatar" src={ picturesUrl + talk.picture } />
                </div>
                <div className="title">
                  <p>{ talk.title }</p>
                </div>
              </div>
              <Onglet
                number={ talk.number - 1 }
                title={ talk.title }
                messages={ talk.messages }
                show={ talk.onglet }
                to={ talk.to }
                talk={ talk.id }
                picture={ talk.picture }
                handleDisplayMessage={ this._handleDisplayMessage }
              />
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default Chat;