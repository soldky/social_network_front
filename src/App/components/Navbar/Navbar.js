import React, { Component } from 'react';
import './Navbar.css';
import { picturesUrl } from '../../../config/connexion_back';

// import material-ui components
import Badge from '@material-ui/core/Badge';
import IconGroup from '@material-ui/icons/Group';
import IconButton from '@material-ui/core/IconButton';
import MailIcon from '@material-ui/icons/Mail';
import MenuItem from '@material-ui/core/MenuItem';
import NotificationsIcon from '@material-ui/icons/Notifications';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuList from '@material-ui/core/MenuList';
import Button from '@material-ui/core/Button';
import Avatar from "@material-ui/core/Avatar/Avatar";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';

// config
import router from "../../../config/router";

// services manager
import user from "../../services/user/User";

// import components
import Logout from '../../components/Logout/Logout';

class Navbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      user_connected: user.get_connected_user()
    };
  }

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl, user_connected } = this.state;
    const isMenuOpen = Boolean(anchorEl);

    const renderMenu = (
      <Popper open={ isMenuOpen } anchorEl={this.anchorEl} transition disablePortal className="popper">
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={this.handleMenuClose}>
                <MenuList>
                  { user_connected._id ? (
                    <a href={ router.profile.replace(":id", user_connected._id) }><MenuItem>Profile</MenuItem></a>
                  ) : (
                    <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
                  )}
                  <MenuItem onClick={this.handleMenuClose}>Paramètres</MenuItem>
                  <Logout />
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    );

    return (
      <div className="wrapper">
        <div className="header-name">
          <a href={ router.main }><h1>SocialNetwork</h1></a>
          <div className="search">
            <div className="searchIcon">
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search …"
              className="inputRoot"
            />
          </div>
        </div>

        <div className="navbar">
          <div className="navbar-icons">
            <IconButton color="inherit" classes={{ label: 'label' }} className="navbar-icon">
              <Badge badgeContent={2} color="secondary">
                <IconGroup />
              </Badge>
              <p>Amis</p>
            </IconButton>
            <IconButton color="inherit" classes={{ label: 'label' }} className="navbar-icon">
              <Badge badgeContent={4} color="secondary">
                <MailIcon />
              </Badge>
              <p>Messagerie</p>
            </IconButton>
            <IconButton color="inherit" classes={{ label: 'label' }} className="navbar-icon">
              <Badge badgeContent={17} color="secondary">
                <NotificationsIcon />
              </Badge>
              <p>Notifications</p>
            </IconButton>
          </div>
          <Button
            className="button-account"
            color="inherit"
            buttonRef={node => {
              this.anchorEl = node;
            }}
            aria-owns={isMenuOpen ? 'menu-list-grow' : undefined}
            aria-haspopup="true"
            onClick={this.handleProfileMenuOpen}
          >
            <Avatar aria-label="Recipe" className="main-avatar" src={ picturesUrl + user_connected.picture }/>
            <span className="name-account">{ user_connected.name.firstname }</span>
            <ArrowDropDownIcon />
          </Button>
          {renderMenu}
        </div>
      </div>
    )
  }

}

export default Navbar;
