import React, { Component } from 'react';
import './Logout.css';

// import material-ui components
import connexionManager from '../../services/user/User';
import MenuItem from "@material-ui/core/MenuItem/MenuItem";

class Logout extends Component {

  logout() {
    connexionManager.logout();

  };

  render() {
    return (
      <MenuItem onClick={ this.logout }>Déconnexion</MenuItem>
    )
  }
}

export default Logout;