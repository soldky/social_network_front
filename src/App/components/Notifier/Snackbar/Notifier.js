import React, { Component } from 'react';

// import material-ui component
import Snackbar from "@material-ui/core/Snackbar/Snackbar";
import { withStyles } from "@material-ui/core";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";
import NotifierTemplate from './NotifierTemplate';
import connect from "react-redux/es/connect/connect";

import { actionCloseNotifier } from "../../../services/store/actions/notifier.actions";

const notifierStyle = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

class Notifier extends Component {

  componentDidUpdate() {
    console.log(this.props);
  }

  handlerClose = () => {
    this.props.dispatch(actionCloseNotifier)
  };

  render() {
    const NotifierWrapper = withStyles(notifierStyle)(NotifierTemplate);
    const { notifier } = this.props;
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={ notifier.show }
        autoHideDuration={ 3000 }
        onClose={ this.handlerClose }
      >
        <NotifierWrapper
          variant={ notifier.variant }
          message={ notifier.message }
          onClose={ this.handlerClose }
        />
      </Snackbar>
    )
  }

}

const mapStateToProps = (state) => ({
  notifier: state.toggleNotifier.notifier
});

export default connect(
  mapStateToProps
)(Notifier);
