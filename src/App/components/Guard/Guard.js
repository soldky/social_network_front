import React  from 'react';
import  { Redirect } from 'react-router-dom'
import PropTypes from 'prop-types';
import router from "../../../config/router";

function Guard({verification}) {
  if (!verification) {
    return <Redirect to={ router.main }  />
  }

  return null;
}

Guard.propTypes = {
  verification: PropTypes.bool.isRequired
};

export default Guard;