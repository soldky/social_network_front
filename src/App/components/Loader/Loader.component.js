import Loader from 'react-loader-spinner'
import React  from 'react';
import './Loader.css';

function LoaderComponent() {
  return(
    <div className="loader">
      <p>Veuillez patienter</p>
      <Loader
         type="ThreeDots"
         color="#273E4A"
         height="200"
         width="200"
      />
    </div>
  )
}

export default LoaderComponent;