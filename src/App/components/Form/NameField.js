import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';

class NameField extends Component {
  static defaultProps = {
    name: 'name',
    placeholder: 'NameField',
    required: false,
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    handleChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = { value: "" }
  }

  handleChange = event => {
    const { handleChange } = this.props;

    let value = event.target.value.charAt(0).toUpperCase() + event.target.value.slice(1).toLowerCase();
    handleChange(event.target.name, event.target.value);
    this.setState({value: value});
  };

  render() {
    const { name, placeholder, required } = this.props;
    return (
      <Input
        name={name}
        onChange={this.handleChange}
        placeholder={placeholder}
        required={required}
        type="text"
        value={this.state.value}
        disableUnderline={true}
        fullWidth={true}
      />
    )
  }
}

export default NameField;