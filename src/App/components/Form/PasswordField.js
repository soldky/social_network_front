import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';

class PasswordField extends Component {
  static defaultProps = {
    name: 'password',
    placeholder: 'PasswordField',
    required: false,
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
    handleChange: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = { value: "" }
  }

  handleChange = event => {
    const { handleChange } = this.props;

    let value = event.target.value;
    handleChange(event.target.name, event.target.value);
    this.setState({value: value});
  };

  render() {
    const { name, placeholder, required } = this.props;
    return (
      <Input
        name={name}
        onChange={this.handleChange}
        placeholder={placeholder}
        required={required}
        type="password"
        value={this.state.value}
        disableUnderline={true}
      />
    )
  }
}

export default PasswordField;