import React, { Component } from 'react';
import './Signin.css';

// import material-ui components
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Button from "@material-ui/core/Button/Button";

// import components
import EmailField from "../Form/EmailField";
import NameField from "../Form/NameField";
import PasswordField from "../Form/PasswordField";

// import services manager
import user from "../../services/user/User";
import connect from "react-redux/es/connect/connect";

class Signin extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  // Arrow fx for bind
  handleChange = (key, value) => {
    this.setState({[key]: value});
  };

  // Arrow fx for bind
  register = (event) => {
    const { firstname, lastname, email, password } = this.state;
    event.preventDefault();

    user.register(firstname, lastname, email, password).then((response) => {
      this.props.dispatch(response);
      this.setState({});

      if(response.type !== "OPEN_NOTIFIER") {
        window.location.reload();
      }
    });
  };

  render() {
    return (
      <div className="signin">
        <form className="form" onSubmit={(event) => this.register(event)}>
          <header className="signin-header">
            <h2>Inscrivez-vous gratuitement</h2>
          </header>
          <section className="signin-contain">
            <FormControl margin="normal">
              <InputLabel
                htmlFor="firstname"
                shrink={false}
                focused={false}>Prénom</InputLabel>
              <NameField
                id="firstname"
                name="firstname"
                placeholder=""
                handleChange={this.handleChange}
                required={true}
                value={this.state.firstname}
              />
            </FormControl>
            <FormControl margin="normal">
              <InputLabel
                htmlFor="lastname"
                shrink={false}
                focused={false}>Nom</InputLabel>
              <NameField
                id="lastname"
                name="lastname"
                placeholder=""
                handleChange={this.handleChange}
                required={true}
                value={this.state.lastname}
              />
            </FormControl>
            <FormControl margin="normal">
              <InputLabel
                htmlFor="email"
                shrink={false}
                focused={false}>E-mail</InputLabel>
              <EmailField
                id="email"
                name="email"
                placeholder=""
                handleChange={this.handleChange}
                required={true}
                value={this.state.email}
              />
            </FormControl>
            <FormControl margin="normal">
              <InputLabel
                htmlFor="password"
                shrink={false}
                focused={false}>Mot de passe</InputLabel>
              <PasswordField
                id="password"
                name="password"
                placeholder=""
                required={true}
                disableUnderline={true}
                handleChange={this.handleChange}
              />
            </FormControl>
            <Button
              type="submit">
              S'inscrire
            </Button>
          </section>
        </form>
      </div>
    )
  }
}

export default connect()(Signin);