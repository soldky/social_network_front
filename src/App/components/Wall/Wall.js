import React, { Component } from 'react';
import Post from "../Post/Post";
import TextField from '@material-ui/core/TextField';
import './Wall.css';
import Button from "@material-ui/core/Button/Button";
import PhotoIcon from '@material-ui/icons/Photo';
import VideocamIcon from '@material-ui/icons/Videocam';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';

class Wall extends Component{

  render() {
    return (
      <div id="wall">
          <form className="form">
            <div className='containForm'>
              <div id="textField-contain">
                <TextField
                  id="standard-multiline-static"
                  InputProps={{ disableUnderline: true }}
                  fullWidth
                  placeholder="Ecrire un post"
                  multiline
                  rows="5"
                  className="textField"
                  margin="normal"
                />
              </div>
              <div className="icon-form">
                <PhotoIcon className="icon" />
                <VideocamIcon className="icon" />
                <InsertEmoticonIcon className="icon" />
              </div>
            </div>
            <Button
              type="submit"
              fullWidth>
              Publier
            </Button>
          </form>
        <Post />
        <Post />
        <Post />
        <Post />
      </div>
    );
  }
}

export default Wall;