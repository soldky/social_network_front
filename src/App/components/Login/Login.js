import React, { Component } from 'react';
import './Login.css';
import { connect } from 'react-redux';

// import material-ui components
import Button from "@material-ui/core/Button/Button";

// import components
import EmailField from "../Form/EmailField";
import PasswordField from "../Form/PasswordField";

// import services manager
import user from "../../services/user/User";

// config
import router from "../../../config/router";

class Login extends Component {

  state = {};

  // Arrow fx for bind
  handleChange = (key, value) => {
    this.setState({[key]: value});
  };

  // Arrow fx for bind
  login = (event) => {
    const { email, password } = this.state;
    event.preventDefault();

    user.login(email, password).then((response) => {
      if(response.type === "OPEN_NOTIFIER") {
        this.props.dispatch(response);
      }
      else {
        this.setState({});
        window.location.reload();
      }
    });
  };

  render() {
    return (
      <div className="wrapper">
        <div className="header-name">
          <a href={ router.main }><h1>SocialNetwork</h1></a>
        </div>
        <form className="form form-login" onSubmit={(event) => this.login(event)}>
          <EmailField
            name="email"
            placeholder="E-mail"
            required={true}
            handleChange={this.handleChange}
            value={this.state.email}
          />
          <PasswordField
            name="password"
            placeholder="Mot de passe"
            required={true}
            type="password"
            disableUnderline={true}
            handleChange={this.handleChange}
            onChange={(event) => this.handleChange(event.target.name, event.target.value)}
          />
          <Button
            type="submit">
            S'identifier
          </Button>
          <a href={ router.forget_password }>Mot de passe oublié ?</a>
        </form>
      </div>
    )
  }

}

export default connect()(Login);