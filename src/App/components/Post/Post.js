import React, { Component } from 'react';
import './Post.css';
import { picturesUrl } from '../../../config/connexion_back';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ThumbUpOutlinedIcon from '@material-ui/icons/ThumbUpOutlined';
import ShareIcon from '@material-ui/icons/Share';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CommentOutlinedIcon from '@material-ui/icons/CommentOutlined';

class Post extends Component{
  state = { expanded: false };

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };

  render() {
    return (
      <Card className="post">
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className="avatar" src={ picturesUrl + "pikachu.png" }/>
          }
          action={
            <IconButton>
              <MoreVertIcon />
            </IconButton>
          }
          title="Shrimp and Chorizo Paella"
          subheader="September 14, 2016"
        />
        <CardMedia
          className="media"
          image={ picturesUrl + "paella.jpg" }
          title="Paella dish"
        />
        <CardContent>
          <Typography component="p">
            This impressive paella is a perfect party dish and a fun meal to cook together with your
            guests. Add 1 cup of frozen peas along with the mussels, if you like.
          </Typography>
        </CardContent>
        <CardActions className="actions" disableActionSpacing>
          <IconButton aria-label="Add to favorites">
            <ThumbUpOutlinedIcon />
            <p>J'aime</p>
          </IconButton>
          <IconButton aria-label="Message">
            <CommentOutlinedIcon />
            <p>Commenter</p>
          </IconButton>
          <IconButton aria-label="Share">
            <ShareIcon />
            <p>Partager</p>
          </IconButton>
        </CardActions>
      </Card>
    );
  }
}

export default Post;