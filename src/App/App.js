import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import Store from './services/store/configureStore';

// import parts
import Header from "./parts/Header/Header";
import Main from "./parts/Main/Main";
import PasswordForgot from "./parts/PasswordForgot/PasswordForgot";
import Profile from "./parts/Profile/Profile";

// config
import router from "../config/router";
import Notifier from "./components/Notifier/Snackbar/Notifier";

class App extends Component {

  render() {
    return (
      <Provider store={ Store }>
        <Notifier />
        <div className="App">
          <Header />
          <Router>
            <Route exact path={ router.main } component={ Main } />
            <Route exact path={ router.forget_password } component={ PasswordForgot } />
            <Route path={ router.profile } component={ Profile } />
          </Router>
        </div>
      </Provider>
    );
  }

}

export default App;
