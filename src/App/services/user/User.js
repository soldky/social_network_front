import instance from "./methods/Token";
import HttpRequestManager from "../httpRequest/HttpRequestManager";

class User {

  is_connected() {
    return instance.is_connected();
  }

  get_connected_user() {
    return instance.connected_user();
  }

  set_connected(authentificate, user) {
    instance.set_connected(authentificate, user);
  }

  set_unconnected() {
    instance.set_unconnected();
  }

  save(user) {
    instance.save(user);
  }

  login = (email, password) => {
    if(email === "test@test" && password === "test") {
      this.set_connected("test");
      window.location.reload();
    }
    else {
      return HttpRequestManager.post("/api/users/login/", {email, password}).then((response) => {
        if (response.status === 200) {
          this.set_connected(response.data.token, response.data.user);

          return true;
        }
      }).catch((error) => {
        return {
          type: "OPEN_NOTIFIER",
          payload: {
            variant: "error",
            message: error.response.status + " - " + error.response.data.text
          }
        };
      });
    }
  };

  logout() {
    this.set_unconnected();
    window.location.reload();
  };

  register(firstname, lastname, email, password) {
    let name = { firstname, lastname };

    return HttpRequestManager.post("/api/users/register/", {name, email, password}).then((response) => {
      if(response.status === 200) {
        this.set_connected(response.data.token, response.data.user);
        return {
          type: "LOGIN",
          payload: response.data.user
        };
      }
    }).catch((error) => {
      return {
        type: "OPEN_NOTIFIER",
        payload: {
          variant: "error",
          message: error.response.status + " - " + error.response.data.text
        }
      };
    });
  };

  reset_password(email) {
    return HttpRequestManager.post("/api/users/reset/password", {email}).then((response) => {
      if(response.status === 200) {
        return {
          type: 'OPEN_NOTIFIER',
          payload: {variant: "info", message: "Mot de passe réinitialisé, merci de consulter vos mails.", show: true}
        };
      }
    }).catch((error) => {
      if(error.response) {
        return {
          type: 'OPEN_NOTIFIER',
          payload: {variant: "error", message: error.response.status + " : " + error.response.data.text, show: true}
        };
      }
      else {
        return {
          type: 'OPEN_NOTIFIER',
          payload: {variant: "info", message: "Votre mot de passe est en train d'être réinitialisé. Vous recevrez un mail dans les prochaines minutes.", show: true}
        };
      }
    });
  }

  get_user(userId) {
    return HttpRequestManager.post("/api/users/", { userId }).then((response) => {
      if(response.status === 200) {
        return response.data.user;
      }
    }).catch(() => {
      return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: "Une erreur est survenue.", show: true}
      };
    });
  }

  add_friend(friendId) {
    return HttpRequestManager.post("/api/users/friends/add_one/", { friendId }).then((response) => {
      if(response.status === 200) {
        return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "info", message: "Ami ajouté.", show: true}
      };
      }
    }).catch(() => {
      return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: "Une erreur est survenue.", show: true}
      };
    });
  }

  remove_friend(friendId) {
    return HttpRequestManager.post("/api/users/friends/remove_one/", { friendId }).then((response) => {
      if(response.status === 200) {
        return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "info", message: "Ami supprimé.", show: true}
      };
      }
    }).catch(() => {
      return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: "Une erreur est survenue.", show: true}
      };
    });
  }

  get_messages(talkId) {
    return HttpRequestManager.post("/api/talks/messages/", { talkId }).then((response) => {
      if(response.status === 200) {
        return response.data.messages.reverse();
      }
    }).catch((error) => {
      if(error.response.status) {
        return [];
      }
      else {
        return {
          type: 'OPEN_NOTIFIER',
          payload: {variant: "error", message: "Une erreur est survenue.", show: true}
        };
      }
    });
  }

  save_message_in_talk(message, talk_id) {
    return HttpRequestManager.post("/api/talks/message/new", { message, talk_id }).then((response) => {
      if(response.status === 200) {
        return true;
      }
    }).catch(() => {
      return {
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: "Une erreur est survenue.", show: true}
      };
    });
  }
}

let user = new User();
export default user
