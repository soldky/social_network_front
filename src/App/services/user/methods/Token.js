// private variables
let _token = Symbol('token');
let _user = Symbol('user');

// private functions
const _setToken = Symbol('setToken');
const _removeToken = Symbol('removeToken');
const _setUser = Symbol('setUser');
const _removeUser = Symbol('removeUser');

class Token {

  constructor() {
    this[_token] = localStorage.getItem('token');
    this[_user] = JSON.parse(localStorage.getItem('user'));
  }

  [_setToken](token) {
    this[_token] = token;
    localStorage.setItem('token', token);
  };

  [_removeToken]() {
    this[_token] = "";
    localStorage.removeItem('token');
  }

  [_setUser](user) {
    this[_user] = user;
    localStorage.setItem('user', JSON.stringify(user));
  };

  [_removeUser]() {
    this[_user] = "";
    localStorage.removeItem('user');
  }

  // Obligatory methods
  is_connected() {
    return !!(this[_token] && this[_token] !== "undefined");
  }

  connected_user() {
    return this[_user] && this[_user] !== "undefined" ? this[_user] : false;
  }

  save(user) {
    this[_setUser](user);
  }

  set_connected(authentificate, user) {
    this[_setToken](authentificate);
    this[_setUser](user);
  }

  set_unconnected() {
    this[_removeToken]();
    this[_removeUser]();
  }
}

let instance = new Token();

export default instance;