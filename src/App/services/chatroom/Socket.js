import io from 'socket.io-client';
import { apiUrl } from '../../../config/connexion_back';

class Socket {
  constructor(user, handler) {
    this.socket = io.connect(apiUrl);
    this.register(user._id, handler);
  }

  unregisterHandler() {
    this.socket.off('message');
  }

  register(userId, handler) {
    this.socket.emit('register', userId);
    handler(this.socket);
  }

  join(id_user, cb) {
    this.socket.emit('join', id_user, cb)
  }

  leave(id_user, cb) {
    this.socket.emit('leave', id_user, cb)
  }

  message(from, to, message) {
    this.socket.emit('message', { from, to, message }, this.callback);
  }

  getChatrooms(cb) {
    this.socket.emit('chatrooms', null, cb)
  }

  getAvailableUsers(cb) {
    this.socket.emit('availableUsers', null, cb)
  }

  callback(message) {
    console.log(message);
  }
}

export default Socket;