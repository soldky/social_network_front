import axios from "axios";
import { apiUrl, headerUrl } from '../../../../config/connexion_back';

// private variables
const _axios = Symbol('axios');

class AxiosManager {

  constructor() {
    this[_axios] = axios.create({
      baseURL: apiUrl,
      timeout: 5000,
      headers: headerUrl,
    });
  }

  get(path, data) {
    return this[_axios].get(path, data);
  }

  post(path, data) {
    return this[_axios].post(path, data);
  }

  put(path, data) {
    return this[_axios].put(path, data);
  }

  update(path, data) {
    return this[_axios].update(path, data);
  }

  delete(path, data) {
    return this[_axios].delete(path, data);
  }

}

let instance = new AxiosManager();

export default instance;