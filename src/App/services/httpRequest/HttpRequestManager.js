import instance from './methods/Axios';

class HttpRequestManager {

  static get(path, data) {
    return instance.get(path, data);
  }

  static post(path, data) {
    return instance.post(path, data);
  }

  static put(path, data) {
    return instance.put(path, data);
  }

  static update(path, data) {
    return instance.update(path, data);
  }

  static delete(path, data) {
    return instance.delete(path, data);
  }

}

export default HttpRequestManager;