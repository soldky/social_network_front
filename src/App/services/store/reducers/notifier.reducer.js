const initialState = { notifier: { variant: "success", message: "", show: false } };

function toggleNotifier(state = initialState, action) {
  let nextState;

  switch (action.type) {
    case 'OPEN_NOTIFIER':
      nextState = {
        ...state,
        notifier: action.payload,
      };
      nextState.notifier.show = true;
      return nextState || state;

    case 'CLOSE_NOTIFIER':
      nextState = {
        ...state,
        notifier: { variant: state.notifier.variant, message: state.notifier.message, show: false }
      };
      return nextState || state;

    default:
      return state;
  }
}

export default toggleNotifier;