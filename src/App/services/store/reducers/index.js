import { combineReducers } from 'redux'
import toggleNotifier from './notifier.reducer'

export default combineReducers({
  toggleNotifier
})