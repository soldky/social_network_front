import React, { Component } from 'react';
import './Main.css';

// import services manager
import ConnexionManager from '../../services/user/User';

// import sub parts
import Signin from "../../components/Signin/Signin";
import Wall from "../../components/Wall/Wall";
import Chat from "../../components/Chat/Chat";

import { backgroundUrl } from '../../../config/connexion_back';

class Main extends Component {

  style = "";

  render() {
    if(!ConnexionManager.is_connected()) this.style = "url(" + backgroundUrl + "main.jpg)";

    return (
      <section className="main-background" style={{ backgroundImage: this.style }}>
        {  ConnexionManager.is_connected() ? (
          <div id="main">
            <Wall />
            <Chat />
          </div>
        ) : (
          <div id="main">
            <Signin />
          </div>
        )}
      </section>
    )
  }
}

export default Main;