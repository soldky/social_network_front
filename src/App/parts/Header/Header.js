import React, { Component } from 'react';
import './Header.css';

// import services manager
import user from '../../services/user/User';

// import components
import Login from "../../components/Login/Login";
import Navbar from "../../components/Navbar/Navbar";

// config
import connect from "react-redux/es/connect/connect";


class Header extends Component {

  state = {
    notifierMessage: {
      variant: "success",
      message: "",
      show: false
    }
  };

  render() {
    return (
      <header className="App-header">
        { user.is_connected() ? (
          <Navbar />
        ) : (
          <Login />
        )}
      </header>
    )
  }
}

export default connect()(Header);
