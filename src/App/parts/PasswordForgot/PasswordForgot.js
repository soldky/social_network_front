import React, { Component } from 'react';
import './PasswordForgot.css';

// import material-ui components
import Button from "@material-ui/core/Button/Button";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";

// import components
import EmailField from "../../components/Form/EmailField";
import LoaderComponent from "../../components/Loader/Loader.component";
import Guard from "../../components/Guard/Guard";

// config
import router from "../../../config/router";
import connect from "react-redux/es/connect/connect";

// import services manager
import user from "../../services/user/User";

class PasswordForgot extends Component {

  constructor(props) {
    super(props);

    this.state = {
      reset: false,
      reset_in_progress: false,
      show_snackbar: false,
      snack_message: {
        variant: "success",
        message: ""
      }
    };
  }

  // Arrow fx for bind
  reset_password = (event) => {
    event.preventDefault();
    const { email } = this.state;

    this.setState({reset_in_progress: true});

    user.reset_password(email).then((response) => {
      this.setState({reset: true});
      this.setState({reset_in_progress: false});
      if(response.type === "OPEN_NOTIFIER") {
        this.props.dispatch(response);
      }
    });
  };

  // Arrow fx for bind
  handleChange = (key, value) => {
    this.setState({[key]: value});
  };

  render() {
    const { reset_in_progress } = this.state;

    return (
      <section id="password-forget">
        <Guard
          verification={ !user.is_connected() }
        />
        <div id="resetPassword">
        { reset_in_progress ? <LoaderComponent /> : (
          <form className="form form-resetpassword" onSubmit={(event) => this.reset_password(event)}>
            <section className="resetpassword-contain">
              <div className="resetPassword-header">
                <h2>Réinitialiser votre mot de passe</h2>
              </div>
              <FormControl margin="normal">
                <InputLabel
                  htmlFor="email"
                  shrink={false}
                  focused={false}>Email</InputLabel>
                <EmailField
                  id="email"
                  name="email"
                  placeholder=""
                  handleChange={this.handleChange}
                  required={true}
                  value={this.state.firstname}
                />
              </FormControl>
              <div className="buttons">
                <Button
                  type="submit">
                  Réinitialiser
                </Button>
                <a href={ router.main }>
                  <Button>
                    Retour
                  </Button>
                </a>
              </div>
            </section>
          </form>
        )}
        </div>
      </section>
    )
  }
}

export default connect()(PasswordForgot);