import React, { Component } from "react";
import './Profile.css';
import user from "../../services/user/User";
import Guard from "../../components/Guard/Guard";
import { picturesUrl } from "../../../config/connexion_back";
import connect from "react-redux/es/connect/connect";

// import material-ui components
import Button from "@material-ui/core/Button/Button";

class Profile extends Component {

  state = {};

  constructor(props) {
    super(props);
    this.id = props.match.params.id;
    this.getUser();
  }

  // fx arrow for bind
  _deleteFriends = () => {
    const { userProfile } = this.state;

    let connected_user = user.get_connected_user();

    if(connected_user.friends.indexOf(this.id)) {
      user.remove_friend(this.id).then((response) => {
        if(response.payload.variant === "info") {
          connected_user.friends.pop(this.id);
          user.save(connected_user);

          userProfile.friends.pop(connected_user._id);
          this.setState({userProfile});
        }
        this.props.dispatch(response);
      });
    }
    else {
      this.props.dispatch({
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: userProfile.name.firstname + " " + userProfile.name.lastname + " ne fait pas parti de vos amis.", show: true}
      });
    }
  };

  // fx arrow for bind
  _addFriends = () => {
    let { userProfile } = this.state;

    let connected_user = user.get_connected_user();

    if(connected_user.friends.indexOf(this.id)) {
      user.add_friend(this.id).then((response) => {
        if(response.payload.variant === "info") {
          connected_user.friends.push(this.id);
          user.save(connected_user);

          userProfile.friends.push(connected_user._id);
          this.setState({userProfile});
        }
        this.props.dispatch(response);
      });
    }
    else {
      this.props.dispatch({
        type: 'OPEN_NOTIFIER',
        payload: {variant: "error", message: userProfile.name.firstname + " " + userProfile.name.lastname + " ne fait pas parti de vos amis.", show: true}
      });
    }
  };

  getUser() {
    user.get_user(this.id).then((response) => {
      if(response.type === "OPEN_NOTIFIER") {
        this.props.dispatch(response);
      }
      else {
        this.setState({userProfile: response});
      }
    })
  }

  render() {
    const { userProfile } = this.state;

    console.log(user.get_connected_user().friends);

    return(
      <section id="profile">
        <Guard
          verification={ user.is_connected() }
        />
        { userProfile && (
          <div id="main-container" style={{background: "url('" + picturesUrl + userProfile.poster + "')"}}>
            <div id="cover-bottom">
              <div id="profile-picture">
                <img src={ picturesUrl + userProfile.picture } alt="Avatar" />
              </div>
              <p id="name">{ userProfile.name.firstname } { userProfile.name.lastname }</p>
            </div>
            { user.get_connected_user()._id !== this.id && (
              <div id="add-friends">
                { user.get_connected_user().friends.indexOf(userProfile._id) > -1 ? (
                  <Button onClick={ this._deleteFriends }>Supprimer des amis</Button>
                ) : (
                  <Button onClick={ this._addFriends }>Demander en ami</Button>
                )}
              </div>
            )}
          </div>
        )}
      </section>
    )
  }

}

export default connect()(Profile)