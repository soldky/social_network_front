const router = {
  "main": "/",
  "profile": "/profile/:id",
  "forget_password": "/user/reset_password"
};

export default router