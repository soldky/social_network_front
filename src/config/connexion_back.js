const apiUrl = 'http://localhost:5000/';
const picturesUrl = apiUrl + 'pictures/';
const backgroundUrl = picturesUrl + 'background/';

const headerUrl = {
  'Accept': 'application/json',
  'Content-Type': ['application/json', 'charset=UTF-8'].join(';'),
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'].join(','),
  'Access-Control-Allow-Headers': 'Content-Type',
  'Access-Control-Expose-Headers': 'X-Debug-Token-Link',
  'Access-Control-Allow-Credentials': true,
  'Authorization': 'Bearer ' + localStorage.getItem('token')
};

export { apiUrl, picturesUrl, headerUrl, backgroundUrl }